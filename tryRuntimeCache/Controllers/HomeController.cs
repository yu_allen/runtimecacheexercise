﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.Caching;

namespace tryRuntimeCache.Controllers
{
    public class HomeController : Controller
    {
        private MemoryCache _cache;
        public HomeController()
        {
            _cache = MemoryCache.Default;
        }
        public ActionResult Index()
        {
            ViewBag.test1 = _cache.Get("test1");
            return View();
        }
        public string SetCache()
        {
            try
            {
                //CacheItemPolicy可用來設定一組快取的相關參數
                CacheItemPolicy myPolicy = new CacheItemPolicy();
                //cache到期時間
                myPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(60.0);

                _cache.Add("test1", "Hello", myPolicy);

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return "Completed!";
        }
    }
}